import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.URL;

public class FormTest {
    @Test
    public void CheckEmptyForms() throws Exception {

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("deviceName", "Pixel5");
        capabilities.setCapability("platformVersion", "12");
        capabilities.setCapability("udid", "emulator-5554");
        capabilities.setCapability("automationName", "UiAutomator2");
        capabilities.setCapability("app", "/Users/Max/Downloads/Android-NativeDemoApp-0.2.1.apk");


//      Устанавливаем и открываем приложение.
        MobileDriver driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
//      Нажимаем Forms в меню
        MobileElement FormsMenuButton = (MobileElement) driver.findElementByXPath("//android.view.ViewGroup[@content-desc=\"Forms\"]/android.view.ViewGroup/android.widget.TextView");
        FormsMenuButton.click();
        Thread.sleep(2000);
//      Нажимаем переключатель Switch на странице Forms.
        MobileElement SwitchButton = (MobileElement) driver.findElementByAccessibilityId("switch");
        SwitchButton.click();
        Thread.sleep(2000);
//      Проверяем текст переключателя.
        MobileElement errorText = (MobileElement) driver.findElementByAccessibilityId("switch-text");
        Assert.assertEquals(errorText.getText(), "Click to turn the switch OFF");

        driver.quit();
    }
}
